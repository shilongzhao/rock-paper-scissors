package org.szhao

import akka.actor.{ActorRef, ActorSystem, Props}
import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import akka.pattern.ask
import akka.util.Timeout
import org.szhao.Bot.{HandShape, Result}
import org.szhao.ScoreBoard.ScoreResult

import scala.concurrent.{ExecutionContext, Future}

/**
 * @author zsh
 *         created 2020/Nov/11
 */
class RestApi(system: ActorSystem, timeout: Timeout) extends RestRoutes {
  override def createArena(): ActorRef = system.actorOf(Props(new Arena))
  override def createScoreBoard(): ActorRef = system.actorOf(Props(new ScoreBoard))

  override implicit val executionContext: ExecutionContext = system.dispatcher
  override implicit val requestTimeout: Timeout = timeout
}

trait RestRoutes extends ArenaApi with ScoreBoardApi with EventMarshalling {
  import StatusCodes._

  def routes: Route = playRoute ~ getResultsRoute ~ resetRoute ~ scoreRoutes

  /**
   * play route
   * @return route
   */
  def playRoute: Route = pathPrefix("play") {
    pathEndOrSingleSlash {
      post {
        entity(as[PlayReq]) {req => {
          HandShape.parse(req.myHand) match {
            case Some(handShape) =>
              onSuccess(play(handShape)) {
                case Result(user, bot, userWins) =>
                  userWins match {
                    case 1 =>
                      complete(201, s"You played $user, I played $bot, you win")
                    case -1 =>
                      complete(418, s"You played $user, I played $bot, you lose")
                    case 0 =>
                      complete(204, s"You played $user, I played $bot, it's a draw")
                  }
              }
            case None =>
             complete(400, s"invalid hand ${req.myHand}")
          }
        }}
      }
    }
  }

  /**
   * get results route
   * @return route
   */
  def getResultsRoute: Route = pathPrefix("results") {
    pathEndOrSingleSlash {
      get {
        onSuccess(getResults) {
          case Some((player, computer)) =>
            complete(OK, GetResultsRep(player, computer))
          case None =>
            complete(NotFound, "No results found")
        }
      }
    }
  }

  /**
   * reset route
   * @return route
   */
  def resetRoute: Route = pathPrefix("reset") {
    pathEndOrSingleSlash {
      post {
        onSuccess(reset) {
          case Some(msg) =>
            complete(OK, msg)
          case None =>
            complete(NotFound, "No bot found")
        }
      }
    }
  }

  /**
   * routes for score board
   * @return routes
   */
  def scoreRoutes: Route =
    pathPrefix("scores" / Segment) { username =>
      pathEndOrSingleSlash {
        post {
          entity(as[ScoreRegisterReq]) { req =>
            onSuccess(registerScore(username, req.score)) {
              case ScoreResult(_, _) =>
                complete(Created, req)
              case _ =>
                complete(BadRequest, "Bad request")
            }
          }
        } ~
        get {
          onSuccess(getScore(username)) {
            case Some(ScoreResult(user, score)) =>
              complete(OK, ScoreReportResp(user, score))
            case None =>
              complete(NotFound, s"no score found for user $username")
          }
        }
      }
  }
}

trait ArenaApi {
  import Bot._

  def createArena(): ActorRef

  implicit val executionContext: ExecutionContext
  implicit val requestTimeout: Timeout

  lazy val arena: ActorRef = createArena()

  def play(handShape: HandShape): Future[Result] = arena.ask(PlayEvent(handShape)).mapTo[Result]

  def getResults: Future[Option[(Int, Int)]] = arena.ask(GetResults).mapTo[Option[(Int, Int)]]

  def reset: Future[Option[String]] = arena.ask(Reset).mapTo[Option[String]]

}

trait ScoreBoardApi {
  import ScoreBoard._
  def createScoreBoard(): ActorRef

  implicit val executionContext: ExecutionContext
  implicit val requestTimeout: Timeout

  lazy val scoreBoard: ActorRef = createScoreBoard()

  def registerScore(user: String = "guest", score: Score): Future[ScoreResult] =
    scoreBoard.ask(RegisterScore(user, score)).mapTo[ScoreResult]

  def getScore(user: String = "guest"): Future[Option[ScoreResult]] =
    scoreBoard.ask(GetScore(user)).mapTo[Option[ScoreResult]]

}