package org.szhao

import akka.actor.ActorSystem
import akka.event.Logging
import akka.http.scaladsl.Http
import akka.http.scaladsl.Http.ServerBinding
import akka.stream.ActorMaterializer
import akka.util.Timeout
import com.typesafe.config.{Config, ConfigFactory}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success}

/**
 * @author zsh
 *         created 2020/Nov/11
 */
object Main extends App with RequestTimeout {


  val config = ConfigFactory.load()
  val host = config.getString("http.host")
  val port = config.getInt("http.port")

  implicit val system: ActorSystem = ActorSystem()
  implicit val ec: ExecutionContext = system.dispatcher

  val api = new RestApi(system, requestTimeout(config)).routes

  implicit val materializer: ActorMaterializer = ActorMaterializer()

  val bindingFuture: Future[ServerBinding] = Http().bindAndHandle(api, host, port)

  val log = Logging(system.eventStream, "rock-paper-scissors")
  bindingFuture.onComplete {
    case Success(serverBinding: ServerBinding) =>
      log.info(s"RestApi bound to ${serverBinding.localAddress} ")
    case Failure(exception)=>
      log.error(exception, "Failed to bind to {}:{}!", host, port)
      system.terminate()
  }
}

trait RequestTimeout {

  import scala.concurrent.duration._

  def requestTimeout(config: Config): Timeout = {
    val t = config.getString("akka.http.server.request-timeout")
    val d = Duration(t)
    FiniteDuration(d.length, d.unit)
  }
}
