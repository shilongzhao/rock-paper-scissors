package org.szhao

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import org.szhao.ScoreBoard.{RegisterScore, Score, ScoreResult}
import spray.json.{DefaultJsonProtocol, RootJsonFormat}

/**
 * @author zsh
 *         created 2020/Nov/12
 */

case class PlayReq(myHand: String)
case class GetResultsRep(player: Int, computer: Int)

case class ScoreReportResp(user: String, score: Score)
case class ScoreRegisterReq(score: Score)

trait EventMarshalling extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val playReqFormat: RootJsonFormat[PlayReq] = jsonFormat1(PlayReq)
  implicit val getResultsRespFormat: RootJsonFormat[GetResultsRep] = jsonFormat2(GetResultsRep)

  implicit val scoreRegisterReq: RootJsonFormat[ScoreRegisterReq] = jsonFormat1(ScoreRegisterReq)
  implicit val scoreReportFormat: RootJsonFormat[ScoreReportResp] = jsonFormat2(ScoreReportResp)
}
