package org.szhao

import akka.actor.{Actor, ActorRef, Props}

/**
 * @author zsh
 *         created 2020/Nov/11
 */
object Arena {

}
class Arena extends Actor {
  import Bot._

  private val DEFAULT_USER = "guest"

  def createBot(userId: String = DEFAULT_USER): ActorRef = context.actorOf(Props(new Bot), userId)

  def notFound(): Unit = sender() ! None

  override def receive: Receive = {
    case cmd @ PlayEvent(_) =>
      def createBotAndPlay(): Unit = {
        val bot = createBot()
        bot forward cmd
      }

      def play(child: ActorRef): Unit =  child forward cmd

      context.child(DEFAULT_USER).fold(createBotAndPlay())(play)

    case GetResults =>
      context.child(DEFAULT_USER).fold(notFound())(child => child forward GetResults)

    case Reset =>
     context.child(DEFAULT_USER).fold(notFound())(child => child forward Reset)
  }
}
