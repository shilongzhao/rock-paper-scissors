package org.szhao

import akka.actor.{Actor, ActorLogging}

import scala.collection.mutable

/**
 * @author zsh
 * created 2020/Nov/13
 */
object ScoreBoard {

  type Score = Int

  case class RegisterScore(user: String = "guest", score: Score)
  case class GetScore(user: String = "guest")
  case class ScoreResult(user: String, score: Score)
}

class ScoreBoard extends Actor with ActorLogging {
  import ScoreBoard._

  val scores: mutable.Map[String, Score] = mutable.Map.empty[String, Score].withDefaultValue(0)

  override def receive: Receive = {
    case RegisterScore(user, score) =>
      log.info(s"updating score of user $user to $score")
      scores.update(user, score)
      sender() ! ScoreResult(user, score)
    case GetScore(user) =>
      scores.get(user) match {
        case None =>
          log.info(s"no score found for user $user")
          sender() ! None
        case Some(score) =>
          log.info(s"got score $score for user $user")
          sender() ! Some(ScoreResult(user, score))
      }
    case _ =>
      log.info("unknown command ignored")
  }
}
