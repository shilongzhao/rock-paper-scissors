package org.szhao

import akka.actor.{Actor, ActorLogging}

import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.util.Random

/**
 * @author zsh
 *         created 2020/Nov/11
 */
object Bot {
  
  object HandShape {
    def parse(s: String): Option[HandShape] = s.toLowerCase match {
      case "rock" => Some(Rock)
      case "paper" => Some(Paper)
      case "scissors" => Some(Scissors)
      case _ => None
    }
  }
  
  sealed trait HandShape extends Comparable[HandShape] {
    def up: HandShape
    def down: HandShape
    override def compareTo(o: HandShape): Int =
      if (o == this.up) -1 else if (o == this.down) 1 else 0
  }

  case object Rock extends HandShape {
    override def up: HandShape = Paper
    override def down: HandShape = Scissors
  }

  case object Paper extends HandShape {
    override def up: HandShape = Scissors
    override def down: HandShape = Rock
  }

  case object Scissors extends HandShape {
    override def up: HandShape = Rock
    override def down: HandShape = Paper
  }

  case class PlayEvent(handShape: HandShape)
  case object GetResults
  case object Reset

  case class Result(user: HandShape, bot: HandShape, userWins: Int)
}

/**
 * A bot is per user, if no user information provided, we use `guest`
 *
 * A bot works by calculating the most frequent user plays. The bot starts playing randomly
 * in the first `threshold` rounds.
 *
 * @param userId bot for user
 * @param threshold threshold to apply strategy
 */
class Bot(userId: String = "guest", threshold: Int = 5) extends Actor with ActorLogging {
  import Bot._

  val HANDS = Seq(Rock, Scissors, Paper)

  val plays: mutable.ListBuffer[Result] = ListBuffer.empty
  var nextHand: HandShape = nextHandPrepare()

  override def receive: Receive = {

    case PlayEvent(handShape) =>
      log.info(s"received user play $handShape")
      val result = Result(handShape, nextHand, handShape.compareTo(nextHand))
      sender() ! result
      plays += result
      nextHand = nextHandPrepare()

    case Reset =>
      log.info(s"received user Reset")
      sender() ! Some("reset done")
      nextHand = nextHandPrepare()
      plays.clear()

    case GetResults =>
      log.info(s"received user GetResults")
      sender() ! Some(summary())

    case _ =>
      sender() ! None
      log.info("Unknown message ignored")
  }

  /**
   * Generate the next hand shape to play for bot, the bot uses a probability model
   * @return hand shape
   */
  private def nextHandPrepare(): HandShape = {
    if (plays.size > threshold) mostPlayedUserHand().up
    else HANDS(Random.nextInt(HANDS.size))
  }

  /**
   * user most favoured play in all history
   * @return hand shape
   */
  private def mostPlayedUserHand(): HandShape = {
    val handCount = plays.groupMapReduce(_.user)(_ => 1)(_ + _)
    handCount.maxBy(_._2)._1
  }

  private def summary():(Int, Int) = (plays.count(_.userWins == 1), plays.count(_.userWins == -1))
}
