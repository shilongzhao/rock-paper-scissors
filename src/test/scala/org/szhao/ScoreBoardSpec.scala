package org.szhao

import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpecLike

/**
 * @author zsh
 *         created 2020/Nov/14
 */
class ScoreBoardSpec extends TestKit(ActorSystem("testScoreBoard"))
  with AnyWordSpecLike
  with Matchers
  with ImplicitSender
  with StopSystemAfterAll {
  import ScoreBoard._
  "The score board " must {
    "return none if there is not record for user" in {
      val scoreBoard = system.actorOf(Props(new ScoreBoard))

      scoreBoard ! GetScore("guest")
      expectMsg(None)
    }

    "return registered score " in {
      val scoreBoard = system.actorOf(Props(new ScoreBoard))

      scoreBoard ! RegisterScore("guest", 10)
      expectMsg(ScoreResult("guest", 10))
    }

    "get score should work correctly" in {
      val scoreBoard = system.actorOf(Props(new ScoreBoard))

      scoreBoard ! RegisterScore("zsh", 10)
      expectMsg(ScoreResult("zsh", 10))

      scoreBoard ! GetScore("non-guest")
      expectMsg(None)

      scoreBoard ! GetScore("zsh")
      expectMsg(Some(ScoreResult("zsh", 10)))
    }
  }
}
