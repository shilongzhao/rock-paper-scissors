package org.szhao

import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.szhao.Bot._

/**
 * @author zsh
 *         created 2020/Nov/11
 */
class BotSpec extends TestKit(ActorSystem("testBot"))
  with AnyWordSpecLike
  with Matchers
  with ImplicitSender
  with StopSystemAfterAll {

  "The bot" must {
    "have zero user wins and bot wins in the beginning" in {
      val botActor = system.actorOf(Props(new Bot))
      botActor ! GetResults
      expectMsg(Some((0, 0)))
    }

    "have correct wins, loses and draws" in {
      val bot = system.actorOf(Props(new Bot))
      val shapes = Seq(Rock, Paper, Scissors)
      val limit = 100
      (1 to limit).map(i => shapes(i % shapes.size)).foreach(s => bot ! PlayEvent(s))

      val results = receiveN(limit)
      results.foreach {
        case Result(u, b, uw) =>
          if (uw == 0) u must be eq b
          else if (uw > 0) u must be eq b.up
          else u must be eq b.down
        case _ =>
          fail()
      }

      bot ! GetResults
      expectMsgPF() {
        case Some((a:Int, b:Int)) =>
          (a + b) must be <= limit
        case _ =>
          fail()
      }
    }

    "have empty results after reset" in {
      val bot = system.actorOf(Props(new Bot))
      val limit = 100
      (1 to limit).foreach(_ => bot ! PlayEvent(Rock))

      receiveN(limit)

      bot ! Reset
      expectMsg(Some("reset done"))
      bot ! GetResults
      expectMsg(Some((0, 0)))
    }

    "win if user keep same hand shape" in {
      val bot = system.actorOf(Props(new Bot(threshold = 3)))
      val limit:Int = 1000
      (1 to limit).foreach(_ => bot ! PlayEvent(Scissors))

      val results = receiveN(limit)

      results.drop(3).foreach {
        case Result(u, b, w:Int) =>
          u must be eq Scissors
          b must be eq Rock
          w must be eq (-1).asInstanceOf[AnyRef]
        case _ =>
          fail()
      }
    }
  }
}
