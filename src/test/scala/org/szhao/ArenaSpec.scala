package org.szhao

import akka.actor.{ActorSystem, Props}
import akka.testkit.{ImplicitSender, TestKit}
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpecLike
import org.szhao.Bot.{GetResults, PlayEvent, Reset, Result, Rock}
/**
 * @author zsh
 *         created 2020/Nov/12
 */
class ArenaSpec extends TestKit(ActorSystem("testArena"))
  with AnyWordSpecLike
  with Matchers
  with ImplicitSender
  with StopSystemAfterAll {

  "The arena" must {
    "start a game successfully" in {
      val arena = system.actorOf(Props(new Arena))
      arena ! PlayEvent(Rock)
      expectMsgPF() {
        case Result(u, _, _) =>
          Rock eq u must be
        case _ =>
          fail()
      }
    }

    "get no result if there's no bot" in {
      val arena = system.actorOf(Props(new Arena))
      arena ! GetResults
      expectMsg(None)
    }

    "cannot reset if there's no bot" in {
      val arena = system.actorOf(Props(new Arena))
      arena ! Reset
      expectMsg(None)
    }
  }

}
