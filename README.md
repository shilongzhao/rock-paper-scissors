# Read Me

start app
```
sbt run
```

start play by 
```
 curl -X POST localhost:5000/play -d '{"myHand":"rock"}'  -H "Content-Type: application/json"
```

get results by 
```
curl -X GET localhost:5000/results
```

reset by 
```
curl -X POST localhost:5000/reset
```

### Score

register score
```
curl -X POST localhost:5000/scores/zhaos -d '{"score":20}'  -H "Content-Type: application/json" 
```

get sore
```
curl -X GET localhost:5000/scores/zhaos
```

### Docker

package
```
sbt docker:publishLocal
[info] Built image rps with tags [0.1]

```

run 

```
docker run --rm -p5000:5000 rps:0.1
```

show images 
```
% docker image ls
REPOSITORY          TAG                 IMAGE ID            CREATED              SIZE
rps                 0.1                 2757d58dec57        About a minute ago   107MB
```
